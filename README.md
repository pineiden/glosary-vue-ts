# Vue 3 + Typescript + Vite

This template should help get you started developing with Vue 3 and Typescript in Vite. The template uses Vue 3 `<script setup>` SFCs, check out the [script setup docs](https://v3.vuejs.org/api/sfc-script-setup.html#sfc-script-setup) to learn more.

## Glosario

Esta aplicación permite consultar un diccionario/glosario a una
API. Está definida para que pueda buscar tanto en términos como en
definiciones. 

Permite, además, búsqueda por *expresiones regulares* lo que habilita
búsquedas exactas y grupales según diversos patrones de texto.

Utiliza, entre sus comoponentes, axios para realizar consultas a la
API REST y también D3 para procesar datasets.

Dispone de una búsqueda por campo de texto y por teclado, lo que
facilta encontrar el concepto que se necesite según.
