export interface Definition {
    id: number,
    concept: string,
    definition: string,
    image?: string,
}
