import {GroupDefinition} from './GroupDefinition'

export interface DatasetGroupDefinition {
    name:string,
    dataset?: Array<GroupDefinition>
}