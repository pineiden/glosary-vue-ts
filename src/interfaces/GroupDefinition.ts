import { Definition } from './Definition'

export interface GroupDefinition {
    letter: string,
    items: Array<Definition>
}
