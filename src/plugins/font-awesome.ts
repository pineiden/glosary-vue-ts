import { fas } from "@fortawesome/free-solid-svg-icons";
import { faInstagram, faTwitter } from "@fortawesome/free-brands-svg-icons";
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core'
import { faFontAwesome } from '@fortawesome/free-brands-svg-icons'

import { 
  faUserSecret, 
  faChevronCircleRight, 
  faChevronCircleLeft,
  faSearch
} from '@fortawesome/free-solid-svg-icons';


library.add(faUserSecret)
library.add(faFontAwesome)
library.add(faChevronCircleRight)
library.add(faChevronCircleLeft)
library.add(faSearch)

export { FontAwesomeIcon };